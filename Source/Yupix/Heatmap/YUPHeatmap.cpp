// Fill out your copyright notice in the Description page of Project Settings.


#include "YUPHeatmap.h"

#include "GameFramework/GameStateBase.h"
#include "GameFramework/PlayerState.h"
#include "GameState\YUPGameInstance.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"


// Sets default values
AYUPHeatmap::AYUPHeatmap()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(GetRootComponent());
}

void AYUPHeatmap::StartRecording()
{
	FTimerHandle DummyHandle;
	GetWorld()->GetTimerManager().SetTimer(DummyHandle, this, &AYUPHeatmap::TrackPlayerPoint, 0.01f, true);
}

// Called when the game starts or when spawned
void AYUPHeatmap::BeginPlay()
{
	Super::BeginPlay();
	
	UYUPGameInstance* YUPGameInstance = Cast<UYUPGameInstance>(GetWorld()->GetGameInstance());

	if (!YUPGameInstance)
	{
		return;
	}

	YUPGameInstance->Initialize();
}

void AYUPHeatmap::TrackPlayerPoint() const
{
	const TArray<APlayerState*> Players = GetWorld()->GetGameState()->PlayerArray;

	for(const APlayerState* Player : Players)
	{
		if (const APawn* PlayerPawn = Player->GetPawn())
		{
			//Substracting 10 so line trace "goes through" the mesh
			const float FinalTraceHeight = StaticMesh->GetComponentLocation().Z - 10.0f;
			const FVector PawnLocation = PlayerPawn->GetActorLocation();
			const FVector FinalTraceLocation = FVector(PawnLocation.X, PawnLocation.Y, FinalTraceHeight);

			TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
			ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldStatic));
			ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_WorldDynamic));

			TArray<AActor*> EmptyArray;
			TArray<FHitResult> HitResults;
			UKismetSystemLibrary::LineTraceMultiForObjects(GetWorld(), PawnLocation, FinalTraceLocation, ObjectTypes, false, EmptyArray, EDrawDebugTrace::None, HitResults, false);

			for(const FHitResult& Hit : HitResults)
			{
				if(Hit.bBlockingHit && Hit.GetActor() == this)
				{
					DrawTarget(Hit.Location);
				}
			}
		}
	}
}

void AYUPHeatmap::DrawTarget(const FVector& Location) const
{
	UYUPGameInstance* YUPGameInstance = Cast<UYUPGameInstance>(GetWorld()->GetGameInstance());

	if (!YUPGameInstance)
	{
		return;
	}

	const FTransform InvertedTransform = UKismetMathLibrary::InvertTransform(StaticMesh->GetComponentTransform());
	const FVector LocalLocation = UKismetMathLibrary::TransformLocation(InvertedTransform, Location);

	//We scale the location by the resolution (size of the mesh) and then add 0.5f to center, no need to center Z
	const FVector ScaledLocalLocation = FVector((LocalLocation.X / Resolution) + 0.5f, (LocalLocation.Y / Resolution) + 0.5f, LocalLocation.Z / Resolution);
	
	YUPGameInstance->HeatmapPoints.Add(FHeatmapPoint(Location, ScaledLocalLocation, Strength, Size));
}

