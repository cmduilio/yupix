﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "YUPGameInstance.h"

#include <fstream>

#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMaterialLibrary.h"
#include "Kismet/KismetRenderingLibrary.h"

// Sets default values
UYUPGameInstance::UYUPGameInstance()
{
}

void UYUPGameInstance::Initialize()
{
	HeatmapMaterialInstance = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), HeatpointMaterial);
	UKismetRenderingLibrary::ClearRenderTarget2D(GetWorld(),RenderTarget2D);
}

void UYUPGameInstance::PrintPoints() const
{
	const FString FilePath = FPaths::ProjectDir() + "export.txt";

	std::ofstream SaveData(TCHAR_TO_UTF8(*FilePath));
	
	for(FHeatmapPoint Point : HeatmapPoints)
	{
		SaveData << TCHAR_TO_UTF8(*Point.WorldLocation.ToString()) << std::endl;
	}
	
	SaveData.close();

	UGameplayStatics::SpawnDecalAtLocation(GetWorld(), DecalMaterial,
		FVector(1000.0f, 1750.0f, 1500.0f),
		FVector(1500.0f, 1750.0f, 0.0f),
		FRotator(-90.0f, 0.0f, 0.0f));

	PrintPointsFromIndex(0);
}

void UYUPGameInstance::PrintPointsFromIndex(int FromIndex) const
{
	FTimerDelegate TimerDelegate;
	TimerDelegate.BindUObject(this, &UYUPGameInstance::PrintPoint, FromIndex);
	FTimerHandle DummyHandle;
	GetWorld()->GetTimerManager().SetTimer(DummyHandle, TimerDelegate, 0.01f, false);
}

void UYUPGameInstance::PrintPoint(int CurrentIndex) const
{
	const FHeatmapPoint Point = HeatmapPoints[CurrentIndex];
	HeatmapMaterialInstance->SetScalarParameterValue(FName("Strength"), Point.Strength);
	HeatmapMaterialInstance->SetScalarParameterValue(FName("Size"), Point.Size);
	HeatmapMaterialInstance->SetVectorParameterValue(FName("Position"), Point.LocalLocation);

	UKismetRenderingLibrary::DrawMaterialToRenderTarget(GetWorld(), RenderTarget2D, HeatmapMaterialInstance);

	if(CurrentIndex < HeatmapPoints.Num() - 1)
	{
		PrintPointsFromIndex(++CurrentIndex);	
	}
}