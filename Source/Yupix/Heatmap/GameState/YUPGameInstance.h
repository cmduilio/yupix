﻿#pragma once

#include "CoreMinimal.h"
#include "YUPGameInstance.generated.h"

USTRUCT(BlueprintType)
struct FHeatmapPoint
{
	GENERATED_BODY()
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FVector WorldLocation;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FVector LocalLocation;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float Strength = 1.0f;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float Size = 0.01f;

	FHeatmapPoint()
	{
		WorldLocation = FVector();
		LocalLocation = FVector();
	}

	FHeatmapPoint(FVector InWorldLocation, FVector InLocalLocation, float InStrength, float InSize)
	{
		WorldLocation = InWorldLocation;
		LocalLocation = InLocalLocation;
		Strength = InStrength;
		Size = InSize;
	}
};

UCLASS()
class YUPIX_API UYUPGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	UYUPGameInstance();
	
	void Initialize();
	
	UFUNCTION(BlueprintCallable)
	void PrintPoints() const;

	UPROPERTY(EditDefaultsOnly)
	TObjectPtr<UMaterialInterface> HeatpointMaterial;
	
	UPROPERTY(EditDefaultsOnly)
	TObjectPtr<UMaterialInterface> DecalMaterial;
	
	UPROPERTY(EditDefaultsOnly)
	TObjectPtr<UTextureRenderTarget2D> RenderTarget2D;
	
	TArray<FHeatmapPoint> HeatmapPoints;

private:

	void PrintPointsFromIndex(int FromIndex) const;
	void PrintPoint(int CurrentIndex) const;
	
	UPROPERTY(Transient)
	TObjectPtr<UMaterialInstanceDynamic> HeatmapMaterialInstance;
};
