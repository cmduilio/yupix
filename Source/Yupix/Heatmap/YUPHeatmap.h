// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "YUPHeatmap.generated.h"

UCLASS()
class YUPIX_API AYUPHeatmap : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AYUPHeatmap();

	UFUNCTION(BlueprintCallable)
	void StartRecording();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(EditDefaultsOnly)
	TObjectPtr<UStaticMeshComponent> StaticMesh;
	
	UPROPERTY(EditDefaultsOnly)
	float Strength = 1.0f;
	
	UPROPERTY(EditDefaultsOnly)
	float Size = 0.01f;

private:
	void TrackPlayerPoint() const;
	void DrawTarget(const FVector& Location) const;

	//This is the size of the platform used, setting 100 as default
	float Resolution = 100.0f;
};
