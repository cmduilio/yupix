// Copyright Epic Games, Inc. All Rights Reserved.

#include "YupixGameMode.h"
#include "YupixCharacter.h"
#include "UObject/ConstructorHelpers.h"

AYupixGameMode::AYupixGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
